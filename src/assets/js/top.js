$(function() {
  // Trigger Accordion FAQ
  $('.js-faqQuestion').click(function () {
    $(this).toggleClass('active').siblings('.js-faqAnswer').slideToggle();
  });   
});